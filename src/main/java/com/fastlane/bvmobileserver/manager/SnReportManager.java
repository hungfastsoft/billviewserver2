package com.fastlane.bvmobileserver.manager;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class SnReportManager extends JdbcDaoSupport{
	@Autowired
	public SnReportManager(DataSource dataSource) {
		setDataSource(dataSource);		
	}
	
	public int countRecordById() {
		String query = "SELECT count(*) FROM costctr";
			
		JdbcTemplate jt = getJdbcTemplate();
		int count = (int) jt.queryForObject(query, Integer.class);
		return count;
	}
}
