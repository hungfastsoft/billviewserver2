package com.fastlane.bvmobileserver;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootApplication(scanBasePackages={"com.fastlane.bvmobileserver"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
@PropertySource("classpath:application.properties")
public class App extends SpringBootServletInitializer{
	@Value("${jdbc.url}")
    private String jdbcUrl;
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(App.class);
    }
	
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
    
    @Bean
    public DataSource dataSource(){
    	DriverManagerDataSource dataSource = new DriverManagerDataSource();
    	dataSource.setUrl(getJdbcUrl());
    	dataSource.setDriverClassName("com.hxtt.sql.dbf.DBFDriver");
    	        
        return dataSource;       	
    }
    
	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	} 
}
