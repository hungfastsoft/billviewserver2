package com.fastlane.bvmobileserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fastlane.bvmobileserver.exception.BadRequestException;
import com.fastlane.bvmobileserver.exception.ErrorInfo;
import com.fastlane.bvmobileserver.exception.NotFoundException;
import com.fastlane.bvmobileserver.service.SNService;

@RestController
@RequestMapping("/api")
public class MobileSNReportController {
	
	@Autowired
	SNService snService;
	
	@GetMapping(value="/snreports/{reportId}")	
	public ModelAndView getSNReports(@RequestHeader("deviceId")String deviceId,@PathVariable("reportId")String reportId) throws NotFoundException, BadRequestException {
		
//		System.out.println("\n\n Test DB count : ===== " + snService.getSnReport());
		if(deviceId.equals("111")){
			throw new NotFoundException("Not found report for device: " + deviceId);
		}else if(deviceId.equals("222")){
			throw new BadRequestException("Bad request for device: " + deviceId);
		}
        
		//success
		String newUrl = "/mobreports/company1/" + reportId + "/" + deviceId + ".json";
        return new ModelAndView("forward:" + newUrl);		
	} 
	
	@ExceptionHandler({NotFoundException.class})
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
	@ResponseBody  
    public ErrorInfo notFoundError(Exception ex) {  
		return new ErrorInfo(ex.getMessage(), 1001);
    }
	
	@ExceptionHandler({BadRequestException.class})
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ResponseBody 
    public ErrorInfo badRequestError(Exception ex) {  
		return new ErrorInfo(ex.getMessage(), 1002);
    }
	
	/**
	 * https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc
	 * @param ex
	 * @return
	 */	
	@ExceptionHandler(Exception.class)
	@ResponseBody 
	public ErrorInfo handleException(Exception ex){
		return new ErrorInfo(ex.getMessage(), 1003);
	}
}
