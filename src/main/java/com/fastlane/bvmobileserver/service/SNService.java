package com.fastlane.bvmobileserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fastlane.bvmobileserver.manager.SnReportManager;

@Service
public class SNService {
	
	@Autowired
	SnReportManager reportMgr;
	
	public int getSnReport(){
		return reportMgr.countRecordById();
	}

}
