package com.fastlane.bvmobileserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND)  // 404
public class NotFoundException extends Exception {
    
	public NotFoundException() {
				
	}

	public NotFoundException(String errorMessage) {
		super(errorMessage);
		// TODO Auto-generated constructor stub
	}
}
