package com.fastlane.bvmobileserver.exception;

public class ErrorInfo {
	private String errorMessage;
	private int systemCode;
		
	public ErrorInfo(String errorMessage, int systemCode) {
		super();
		this.errorMessage = errorMessage;
		this.systemCode = systemCode;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(int systemCode) {
		this.systemCode = systemCode;
	}
}
